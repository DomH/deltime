#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGuiApplication>
#include <QScreen>


// Constructeur et destructeur
//
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // Setup de l'IHM
    ui->setupUi(this);
    this->setWindowTitle("delTime");
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    this->setGeometry((screenGeometry.width()-this->width())/2, (screenGeometry.height()-this->height())/2, this->width(), this->height());

    // Mise en place des connections - Attention, bug à l'init dû à double appel à fonction
    connect (ui->startDateEdit, &QDateEdit::userDateChanged, this, [=](){calculDateResultante();});
    connect (ui->decalSpinbox, &QSpinBox::valueChanged, this, [=](){calculDateResultante();});
    connect (ui->inDate1, &QDateEdit::userDateChanged, this, [=](){calculDecalage();});
    connect (ui->inDate2, &QDateEdit::userDateChanged, this, [=](){calculDecalage();});
    ui->startDateEdit->setDate(QDate::currentDate());
}
MainWindow::~MainWindow()
{
    delete ui;
}


// Calcul de date résultante en fonction d'une date d'entrée et d'un décalage
//
void MainWindow::calculDateResultante()
{
    QDate newDTime = ui->startDateEdit->date().addDays(ui->decalSpinbox->value());
    ui->endDateEdit->setDate(newDTime);
    ui->decalSpinbox->setFocus();
}


// Calcul de décalage temporel (en jours) entre 2 dates d'entrée
//
void MainWindow::calculDecalage()
{
    int decal = ui->inDate1->date().daysTo(ui->inDate2->date());
    ui->decalResSpinbox->setValue(decal);
    ui->decalResSpinbox->setFocus();
}

